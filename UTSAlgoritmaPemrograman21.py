print('***************************************')
print('\tTRANSKIP NILAI')
print('***************************************')

class Mahasiswa():
    
    def __init__(self, namaMahasiswa, prodiMahasiswa, nilaiAkhir, keteranganMahasiswa, nilaiTugas, nilaiUTS, nilaiUAS, konversiNilai):

        self.namaMahasiswa = namaMahasiswa
        self.prodiMahasiswa = prodiMahasiswa
        self.nilaiAkhir = nilaiAkhir
        self.keteranganMahasiswa = keteranganMahasiswa
        self.nilaiTugas = nilaiTugas
        self.nilaiUTS = nilaiUTS
        self.nilaiUAS = nilaiUAS
        self.konversiNilai = konversiNilai
    def inputData(self):
        self.namaMahasiswa = input("Nama             : ")
        self.prodiMahasiswa = input("Prodi            : ")
        self.nilaiTugas = int(input("Nilai Tugas      : "))
        self.nilaiUTS = int(input("Nilai UTS        : "))
        self.nilaiUAS = int(input("Nilai UAS        : "))
        print("-------------------------------------")
        self.nilaiTugas *= 0.3
        self.nilaiUTS *= 0.3
        self.nilaiUAS *= 0.4
        self.nilaiAkhir = self.nilaiTugas + self.nilaiUTS + self.nilaiUAS
        self.keteranganMahasiswa = ""
        self.konversiNilai = ""
        if self.nilaiAkhir >= 90:
            self.konversiNilai = "A"
            self.keteranganMahasiswa = "Lulus"
        elif self.nilaiAkhir >= 70 and self.nilaiAkhir <= 89:
            self.konversiNilai = "B"
            self.keteranganMahasiswa = "Lulus"
        elif self.nilaiAkhir >= 59 and self.nilaiAkhir <= 69:
            self.konversiNilai = "C"
            self.keteranganMahasiswa = "Lulus"
        elif self.nilaiAkhir < 50 and self.nilaiAkhir == 0:
            self.konversiNilai = "D"
            self.keteranganMahasiswa = "Tidak Lulus" 
        else :
            self.konversiNilai = "E"
            self.keteranganMahasiswa = "Tidak Lulus"
class Biodata(Mahasiswa):
    def displayData(self):
        print("=====================================")
        print("Nama             : ", self.namaMahasiswa)
        print("Prodi            : ", self.prodiMahasiswa)
        print("Nilai Matkul     : ", self.nilaiAkhir, "(", self.konversiNilai, ")")
        print("Keterangan       : ", self.keteranganMahasiswa)
        print("=====================================\n")

# Program Utama
jumlahMahasiswa = []
jumlahMahasiswa = int(input("Jumlah Mahasiswa : "))
print("=====================================")
print("-------------------------------------")
for x in range(jumlahMahasiswa):
    mhs1 = Biodata("namaMahasiswa", "prodiMahasiswa", "nilaiAkhir", "keteranganMahasiswa", "nilaiTugas", "nilaiUTS", "nilaiUAS", "konversiNilai")
    mhs1.inputData()

for i in range(jumlahMahasiswa):
    mhs1.displayData()